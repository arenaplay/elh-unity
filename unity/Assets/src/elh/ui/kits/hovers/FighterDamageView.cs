﻿// *******************************************************************************
// *  Copyright (c) 2017 Max Stankevich. All Rights Reserved.
// *
// *  NOTICE: You are permitted to use, modify, and distribute this file
// *  in accordance with the terms of the license agreement accompanying it.
// ******************************************************************************/

using elh.ui.common;
using elh.utils;
using UnityEngine;
using UnityEngine.UI;

namespace elh.ui.kits.hovers
{
	public class FighterDamageView : UState<FighterDamageView.Data>
	{
		public Image backImage;
		public Text txtDamage;
		public FollowView followView;

		/// <inheritdoc />
		protected override void OnInitialize()
		{
			base.OnInitialize();
			Require(ref followView);
		}

		/// <inheritdoc />
		protected override void OnRender()
		{
			txtDamage.SetText(state.damage.ToString());
			var euler = backImage.transform.eulerAngles;
			/*euler.z = 90f * Random.Range(-1f, 1f);
			backImage.transform.eulerAngles = euler;*/
			followView.Render();
		}

		/// <inheritdoc />
		protected override void OnStateUpdate()
		{
			base.OnStateUpdate();
			{
				var next = followView.State;
				next.follow = state.follow;
				followView.State = next;
			}
		}

		public struct Data
		{
			public object actor;
			public float damage;
			public Transform follow;
		}
	}
}
