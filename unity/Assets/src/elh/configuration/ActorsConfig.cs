﻿// *******************************************************************************
// *  Copyright (c) 2017 Max Stankevich. All Rights Reserved.
// *
// *  NOTICE: You are permitted to use, modify, and distribute this file
// *  in accordance with the terms of the license agreement accompanying it.
// ******************************************************************************/

using elh.game.entities;
using UnityEngine;

namespace elh.configuration
{
	[FilePath("configs/actors-config")]
	[CreateAssetMenu(fileName = "actors-config", menuName = "ELH/configs/ActorsConfig")]
	public class ActorsConfig : ScriptableConfig
	{
		public ActorEntity[] actors;
	}
}
