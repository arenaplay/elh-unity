﻿// *******************************************************************************
// *  Copyright (c) 2017 Max Stankevich. All Rights Reserved.
// *
// *  NOTICE: You are permitted to use, modify, and distribute this file
// *  in accordance with the terms of the license agreement accompanying it.
// ******************************************************************************/

using System;
using RAIN.Action;
using RAIN.Core;
using RAIN.Representation;
using UnityEngine;
using Random = UnityEngine.Random;

namespace elh.rain.actions
{
	[RAINAction("Wander")]
	public class Wander : RAINAction
	{
		/// <summary> Home of the actor where it should return. </summary>
		public Expression home = new Expression();

		/// <summary> Max distance from the home. </summary>
		public Expression maxHomeDistance = new Expression();

		/// <summary> Max distance of the movement per time. </summary>
		public Expression maxWanderDistance = new Expression();

		/// <summary> Name of the variable in memory that store wander position. </summary>
		public Expression moveTarget = new Expression();

		/// <summary> Whether actor should stay on navigation graph. </summary>
		public Expression stayOnGraph = new Expression();

		/// <summary> Initialize new instance of <see cref="Wander"/>. </summary>
		public Wander()
		{ //
			maxWanderDistance.SetConstant(2f);
			moveTarget.SetVariable("moveTarget");
		}

		/// <inheritdoc />
		public override ActionResult Execute(AI ai)
		{
			if (!moveTarget.IsVariable)
				throw new Exception("Select valid moveTarget variable!");

			var memory = ai.WorkingMemory;
			var kinematic = ai.Kinematic;
			var deltaTime = ai.DeltaTime;

			var distance = 0f;
			if (maxWanderDistance.IsValid)
				distance = maxWanderDistance.Evaluate<float>(deltaTime, memory);

			var position = kinematic.Position;
			var direction = new Vector3(Random.Range(-1f, 1f), 0f, Random.Range(-1f, 1f));
			var nextPosition = position + direction * distance;

			memory.SetItem(moveTarget.VariableName, nextPosition);
			return ActionResult.SUCCESS;
		}
	}
}
