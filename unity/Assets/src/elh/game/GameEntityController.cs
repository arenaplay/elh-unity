﻿// *******************************************************************************
// *  Copyright (c) 2017 Max Stankevich. All Rights Reserved.
// *
// *  NOTICE: You are permitted to use, modify, and distribute this file
// *  in accordance with the terms of the license agreement accompanying it.
// ******************************************************************************/

using elh.configuration;
using elh.game.entities;

namespace elh.game
{
	public class GameEntityController
	{
		public ActorEntity FindActor(string actorID)
		{
			var actors = SharedConfig<ActorsConfig>.shared.actors;
			for (var i = 0; i < actors.Length; i++)
				if (actors[i].ID == actorID)
					return actors[i];

			return null;
		}

		public ItemEntity FindItem(string itemID)
		{
			var items = SharedConfig<ItemsConfig>.shared.items;
			for (var i = 0; i < items.Count; i++)
				if (items[i].ID == itemID)
					return items[i];

			return null;
		}
	}
}
