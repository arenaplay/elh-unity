// *******************************************************************************
// *  Copyright (c) 2017 Max Stankevich. All Rights Reserved.
// *
// *  NOTICE: You are permitted to use, modify, and distribute this file
// *  in accordance with the terms of the license agreement accompanying it.
// ******************************************************************************/

using System;

namespace elh.game.entities
{
	[Serializable]
	public class RecipeEntity
	{
		public string id;
		public ConsumeEntry[] consumes;
		public RewardEntity[] rewards;
	}
}
