﻿// *******************************************************************************
// *  Copyright (c) 2017 Max Stankevich. All Rights Reserved.
// *
// *  NOTICE: You are permitted to use, modify, and distribute this file
// *  in accordance with the terms of the license agreement accompanying it.
// ******************************************************************************/

using System;
using System.Collections;
using System.Collections.Generic;
using strange.extensions.signal.impl;
using UnityEngine;

namespace elh.game.inventory
{
	[Serializable]
	public class Inventory : IEnumerable
	{
		[SerializeField]
		public List<InventoryItem> items;

		[SerializeField]
		public int slotCapacity = 24;

		public readonly Signal<Inventory> onChanged;

		public Inventory()
		{ //
			items = new List<InventoryItem>();
			onChanged = new Signal<Inventory>();
		}

		/// <summary> Total number of items in inventory. </summary>
		public int TotalCount
		{
			get
			{
				var total = 0;
				for (int i = 0, imax = items.Count; i < imax; i++)
					total += items[i].amount;

				return total;
			}
		}

		/// <summary> Number of items of the same type. </summary>
		/// <param name="itemID"></param>
		public int CountOf(string itemID)
		{
			var amount = 0;
			for (int i = 0, imax = items.Count; i < imax; i++)
				if (items[i].itemID == itemID)
					amount += items[i].amount;

			return amount;
		}

		public void Add(string itemID, int amount = 1)
		{
			for (int i = 0, imax = items.Count; i < imax && amount > 0; i++)
			{
				if (items[i].itemID != null)
					continue;

				amount--;
				items[i] = new InventoryItem(itemID);
			}

			for (; amount > 0; amount--)
				items.Add(new InventoryItem(itemID));

			onChanged.Dispatch(this);
		}

		public bool RemoveAt(int index, int amount = 1)
		{
			var item = items[index];
			if (item.amount < amount)
				return false;

			item.amount -= amount;
			if (item.amount > 0) items[index] = item;
			else items[index] = default(InventoryItem);

			onChanged.Dispatch(this);
			return true;
		}

		public bool Remove(string itemID, int amount = 1)
		{
			var count = CountOf(itemID);
			if (count < amount)
				return false;

			for (var i = items.Count - 1; i >= 0 && amount > 0; i--)
			{
				var item = items[i];
				if (item.itemID != itemID)
					continue;

				if (item.amount <= amount)
				{
					amount -= item.amount;
					item = default(InventoryItem);
				}
				else
				{
					item.amount -= amount;
				}

				items[i] = item;
			}

			onChanged.Dispatch(this);
			return true;
		}

		/// <inheritdoc />
		public IEnumerator GetEnumerator()
		{
			return items.GetEnumerator();
		}
	}

	public struct InventoryItem
	{
		public string itemID;
		public int amount;

		/// <inheritdoc />
		public InventoryItem(string itemID)
		{
			this.itemID = itemID;
			amount = 1;
		}
	}
}
