﻿// *******************************************************************************
// *  Copyright (c) 2017 Max Stankevich. All Rights Reserved.
// *
// *  NOTICE: You are permitted to use, modify, and distribute this file
// *  in accordance with the terms of the license agreement accompanying it.
// ******************************************************************************/

using System;
using UnityEngine;

namespace elh.scripts
{
	public class AttachPoints : MonoBehaviour
	{
		public Point[] points;

		public Point GetPointByName(string value)
		{
			for (int i = 0, imax = points.Length; i < imax; i++)
			{
				var point = points[i];
				if (value == point.name)
					return point;
			}
			return default(Point);
		}

		[Serializable]
		public struct Point
		{
			public string name;
			public GameObject point;
		}
	}
}
