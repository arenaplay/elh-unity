// *******************************************************************************
// *  Copyright (c) 2017 Max Stankevich. All Rights Reserved.
// *
// *  NOTICE: You are permitted to use, modify, and distribute this file
// *  in accordance with the terms of the license agreement accompanying it.
// ******************************************************************************/

using elh.battle;
using elh.mvcs.stage.events;
using elh.mvcs.ui.core;
using elh.mvcs.ui.views;
using strange.extensions.dispatcher.eventdispatcher.api;

namespace elh.mvcs.ui.mediators
{
	public class FighterDamageUIMediator : ViewController
	{
		protected FighterDamageUI view;

		[Inject]
		public FighterDamageUI View { get { return view; } set { view = value; } }

		/// <inheritdoc />
		public override void OnRegister()
		{
			base.OnRegister();
			dispatcher.AddListener(ActorAttackEvent.ACTOR_ATTACKED, OnActorAttacked);
		}

		/// <inheritdoc />
		protected override void OnRender() { }

		private void OnActorAttacked(IEvent payload)
		{
			var e = (ActorAttackEvent)payload.data;
			var index = Stat.IndexOf(e.affect, StatType.HEALTH);
			if (index < 0) return;

			var damage = -e.affect[index].value;
			view.viewRenderer.ShowDamage(e.defender, damage);
		}
	}
}
