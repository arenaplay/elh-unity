﻿// *******************************************************************************
// *  Copyright (c) 2017 Max Stankevich. All Rights Reserved.
// *
// *  NOTICE: You are permitted to use, modify, and distribute this file
// *  in accordance with the terms of the license agreement accompanying it.
// ******************************************************************************/

using elh.events;
using elh.mvcs.ui.events;
using elh.mvcs.ui.views;
using elh.utils;
using strange.extensions.mediation.impl;

namespace elh.mvcs.ui.mediators
{
	public class MainDisplayUIMediator : EventMediator
	{
		protected MainDisplayUI view;

		[Inject]
		public MainDisplayUI View { get { return view; } set { view = value; } }

		/// <inheritdoc />
		public override void OnRegister()
		{
			base.OnRegister();

			view.inventoryButton.RequireComponent<EventSystemListener>()
				.onClick.AddListener(x => dispatcher.Dispatch(WindowEvents.OPEN_INVENTORY_EVENT));
		}
	}
}
