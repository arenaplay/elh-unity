﻿// *******************************************************************************
// *  Copyright (c) 2017 Max Stankevich. All Rights Reserved.
// *
// *  NOTICE: You are permitted to use, modify, and distribute this file
// *  in accordance with the terms of the license agreement accompanying it.
// ******************************************************************************/

using elh.battle;
using elh.mvcs.stage.controllers;
using elh.mvcs.stage.mediators;
using elh.mvcs.stage.views;
using strange.extensions.context.api;
using strange.extensions.context.impl;
using UnityEngine;

namespace elh.mvcs.stage
{
	public class StageContext : MVCSContext
	{
		public StageContext() { }
		public StageContext(MonoBehaviour view) : base(view) { }
		public StageContext(MonoBehaviour view, ContextStartupFlags flags) : base(view, flags) { }
		public StageContext(MonoBehaviour view, bool autoMapping) : base(view, autoMapping) { }

		protected override void mapBindings()
		{
			base.mapBindings();

			// controllers
			injectionBinder.Bind<BattleController>().ToSingleton();
			injectionBinder.Bind<LootController>().ToSingleton().CrossContext();
			injectionBinder.Bind<SpawnController>().ToSingleton().CrossContext();

			// mediators
			mediationBinder.BindView<NPCView>().ToMediator<NPCMediator>();
			mediationBinder.BindView<PlayerView>().ToMediator<PlayerMediator>();
			mediationBinder.BindView<CameraView>().ToMediator<CameraMediator>();
			mediationBinder.BindView<SurfaceView>().ToMediator<SurfaceMediator>();
			mediationBinder.BindView<SpawnView>().ToMediator<SpawnMediator>();
			mediationBinder.BindView<LootView>().ToMediator<LootMediator>();
			mediationBinder.BindView<TravelPointView>().ToMediator<TravelPointMediator>();
			mediationBinder.BindView<InteractiveView>().ToMediator<InteractiveMediator>();

			mediationBinder.BindView<FighterView>()
						   .ToMediator<FighterMediator>()
						   .ToMediator<FighterLootMediator>();
		}
	}
}
