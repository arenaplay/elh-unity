﻿// *******************************************************************************
// *  Copyright (c) 2017 Max Stankevich. All Rights Reserved.
// *
// *  NOTICE: You are permitted to use, modify, and distribute this file
// *  in accordance with the terms of the license agreement accompanying it.
// ******************************************************************************/

using System;
using elh.mvcs.stage.views;
using elh.utils;
using RAIN.Core;
using RAIN.Memory;
using UnityEngine;

namespace elh.mvcs.stage.components
{
	public class AIComponent : MonoBehaviour
	{
		[NonSerialized]
		public RAINMemory memory;

		public AIRig aiRig;
		public GameObject body;

		public bool IsDead
		{
			get { return memory.GetItem<bool>(MemoryVars.DEAD); }
			set { memory.SetItem(MemoryVars.DEAD, value); }
		}

		public bool IsBusy
		{
			get { return memory.GetItem<bool>(MemoryVars.BUSY); }
			set { memory.SetItem(MemoryVars.BUSY, value); }
		}

		public bool CanAttack
		{
			get { return memory.GetItem<bool>(MemoryVars.CAN_ATTACK); }
			set { memory.SetItem(MemoryVars.CAN_ATTACK, value); }
		}

		public ActorView InteractTarget
		{
			get { return memory.GetItem(MemoryVars.INTERACT_TARGET) as ActorView; }
			set
			{
				if (value == null) memory.RemoveItem(MemoryVars.INTERACT_TARGET);
				else memory.SetItem(MemoryVars.INTERACT_TARGET, value);
			}
		}

		public ActorView AttackTarget
		{
			get { return memory.GetItem(MemoryVars.ATTACK_TARGET) as ActorView; }
			set
			{
				if (value == null) memory.RemoveItem(MemoryVars.ATTACK_TARGET);
				else memory.SetItem(MemoryVars.ATTACK_TARGET, value);
			}
		}

		public void SetMoveTarget(Vector3 position)
		{
			memory.SetItem(MemoryVars.MOVE_TARGET, position);
			SetCloseDistance(null);
		}

		public void SetMoveTarget(Component view)
		{
			SetCloseDistance(0.5f);
			if (view) memory.SetItem(MemoryVars.MOVE_TARGET, view.gameObject);
			else memory.RemoveItem(MemoryVars.MOVE_TARGET);
		}

		public void SetFaceTarget(Component view)
		{
			if (view) memory.SetItem(MemoryVars.FACE_TARGET, view.gameObject);
			else memory.RemoveItem(MemoryVars.FACE_TARGET);
		}

		public void SetCloseDistance(float? distance)
		{
			if (distance == null) memory.RemoveItem(MemoryVars.CLOSE_DISTANCE);
			else memory.SetItem(MemoryVars.CLOSE_DISTANCE, distance.Value * 0.9f);
		}

		public void SetRange(float? range)
		{
			SetCloseDistance(range);
			if (range == null) memory.RemoveItem(MemoryVars.RANGE);
			else memory.SetItem(MemoryVars.RANGE, range.Value);
		}

		/// <inheritdoc />
		protected void Awake()
		{
			if (body == null)
				body = gameObject;

			this.ComponentRef(ref aiRig, true);
			if (aiRig == null) return;
			var ai = aiRig.AI;

			ai.Body = body;
			memory = ai.WorkingMemory;
		}

		/// <inheritdoc />
		protected void Start()
		{
			if (aiRig == null) return;
			var ai = aiRig.AI;
			ai.Body = body;
		}
	}
}
