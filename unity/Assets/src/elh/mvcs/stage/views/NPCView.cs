// *******************************************************************************
// *  Copyright (c) 2017 Max Stankevich. All Rights Reserved.
// *
// *  NOTICE: You are permitted to use, modify, and distribute this file
// *  in accordance with the terms of the license agreement accompanying it.
// ******************************************************************************/

using elh.mvcs.stage.components;
using elh.mvcs.stage.mediators;
using elh.utils;

namespace elh.mvcs.stage.views
{
	public class NPCView : ActorView
	{
		public AIComponent ai;
		public FighterView fighterView;
		public BattleUnitComponent battleUnit;

		protected override void Awake()
		{
			base.Awake();
			this.RequireComponentRef(ref ai);
			this.RequireComponentRef(ref fighterView);
			this.RequireComponentRef(ref battleUnit);
			ai.body = gameObject;
		}
	}
}
