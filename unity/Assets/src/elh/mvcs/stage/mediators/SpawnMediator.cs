﻿// *******************************************************************************
// *  Copyright (c) 2017 Max Stankevich. All Rights Reserved.
// *
// *  NOTICE: You are permitted to use, modify, and distribute this file
// *  in accordance with the terms of the license agreement accompanying it.
// ******************************************************************************/

using System.Collections.Generic;
using elh.mvcs.stage.controllers;
using elh.mvcs.stage.views;
using strange.extensions.mediation.impl;

namespace elh.mvcs.stage.mediators
{
	public class SpawnMediator : EventMediator
	{
		protected List<ActorView> actors;

		[Inject]
		public SpawnView View { get; set; }

		[Inject]
		public SpawnController SpawnController { get; set; }

		public SpawnMediator()
		{ //
			actors = new List<ActorView>();
		}

		/// <inheritdoc />
		public override void OnRegister()
		{
			base.OnRegister();
			View.onSpawn.AddListener(Spawn);
		}

		protected void Spawn(SpawnView spawn)
		{
			if (actors.Count >= spawn.maxCount)
				return;

			var index = actors.Count;
			var actorView = SpawnController.SpawnActor(spawn.actorID, spawn.transform);
			actorView.name += "-" + index;
			actorView.onDestroy.AddOnce(OnActorDestroy);
			actors.Add(actorView);
		}

		private void OnActorDestroy(ActorView actorView)
		{ //
			actors.Remove(actorView);
		}
	}
}
