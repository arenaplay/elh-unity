﻿// *******************************************************************************
// *  Copyright (c) 2017 Max Stankevich. All Rights Reserved.
// *
// *  NOTICE: You are permitted to use, modify, and distribute this file
// *  in accordance with the terms of the license agreement accompanying it.
// ******************************************************************************/

using elh.mvcs.stage.controllers;
using elh.mvcs.stage.events;
using elh.mvcs.stage.views;
using strange.extensions.mediation.impl;
using UnityEngine;

namespace elh.mvcs.stage.mediators
{
	public class ActorMediator : EventMediator
	{
		protected ActorView view;

		[Inject]
		public SpawnController SpawnController { get; set; }

		/// <inheritdoc />
		public override void OnRegister()
		{
			base.OnRegister();
			SpawnController.InitializeActor(view);
		}

		/// <inheritdoc />
		protected virtual void Start()
		{
			view.eventListener.onClick.AddListener(x =>
			{
				Debug.Log("Actor click: " + view);
				dispatcher.Dispatch(PlayerEvent.ACTOR_CLICK, view);
			});
		}
	}
}
