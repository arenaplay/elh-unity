﻿// *******************************************************************************
// *  Copyright (c) 2017 Max Stankevich. All Rights Reserved.
// *
// *  NOTICE: You are permitted to use, modify, and distribute this file
// *  in accordance with the terms of the license agreement accompanying it.
// ******************************************************************************/

using elh.game.entities;
using elh.mvcs.stage.controllers;
using elh.mvcs.stage.views;

namespace elh.mvcs.stage.mediators
{
	public class InteractiveMediator : ActorMediator
	{
		protected new InteractiveView view;

		[Inject]
		public InteractiveView View
		{
			get { return view; }
			set
			{
				view = value;
				base.view = view;
			}
		}

		[Inject]
		public LootController LootController { get; set; }

		/// <inheritdoc />
		public override void OnRegister()
		{
			base.OnRegister();
			view.onCraftDone.AddListener(OnCraftDone);
		}

		private void OnCraftDone(ActorView byActor, CraftEntity craft)
		{
			var position = byActor.transform.position;
			LootController.SpawnLoot(craft.rewards, position, 0.25f);
		}
	}
}
