// *******************************************************************************
// *  Copyright (c) 2017 Max Stankevich. All Rights Reserved.
// *
// *  NOTICE: You are permitted to use, modify, and distribute this file
// *  in accordance with the terms of the license agreement accompanying it.
// ******************************************************************************/

using System;

namespace elh.configuration
{
	public static class ConfigUtils
	{
		public static string GetConfigPath(Type configType)
		{
			var attributes = configType.GetCustomAttributes(typeof(FilePathAttribute), true);
			if (attributes == null || attributes.Length < 1)
				throw new InvalidOperationException(string.Format("config '{0}' does not provide file attribute!", configType));

			var attribute = (FilePathAttribute)attributes[0];
			return attribute.Path;
		}
	}
}
