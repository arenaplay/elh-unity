﻿// *******************************************************************************
// *  Copyright (c) 2017 Max Stankevich. All Rights Reserved.
// *
// *  NOTICE: You are permitted to use, modify, and distribute this file
// *  in accordance with the terms of the license agreement accompanying it.
// ******************************************************************************/

using System;

namespace elh.configuration
{
	[AttributeUsage(AttributeTargets.Class)]
	public class FilePathAttribute : Attribute
	{
		/// <summary> Path to the file. </summary>
		public string Path { get; set; }

		public FilePathAttribute(string path)
		{
			if (string.IsNullOrEmpty(path))
				throw new ArgumentOutOfRangeException("path", path, "value is empty!");

			Path = path;
		}
	}
}
