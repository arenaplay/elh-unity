﻿// *******************************************************************************
// *  Copyright (c) 2017 Max Stankevich. All Rights Reserved.
// *
// *  NOTICE: You are permitted to use, modify, and distribute this file
// *  in accordance with the terms of the license agreement accompanying it.
// ******************************************************************************/

using System;

namespace elh.ui.support
{
	public abstract class Renderer : IDisposable, IRenderer
	{
		protected bool dirty;
		protected bool destroyed;
		protected bool initialized;

		/// <inheritdoc />
		public void Initialize()
		{
			if (initialized) return;
			OnInitialize();
			initialized = true;
		}

		/// <inheritdoc />
		public void Render()
		{
			dirty = false;
			OnWillRender();
			OnRender();
		}

		/// <inheritdoc />
		public void Update()
		{
			if (!dirty) return;
			dirty = false;
			OnWillRender();
			OnRender();
		}

		public void SetDirty()
		{ //
			dirty = true;
		}

		/// <inheritdoc />
		public void Dispose()
		{
			if (destroyed) return;
			OnDispose();
		}

		protected virtual void OnInitialize() { }

		/// <summary>
		/// Occurs immediately before rendering when new props or state are being received.
		/// Use this as an opportunity to perform preparation before an update occurs.
		/// This method is not called for the initial render.
		/// </summary>
		protected virtual void OnWillRender() { }

		protected abstract void OnRender();

		protected virtual void OnDispose() { }
	}
}
