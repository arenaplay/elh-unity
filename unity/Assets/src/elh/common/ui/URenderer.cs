// *******************************************************************************
// *  Copyright (c) 2017 Max Stankevich. All Rights Reserved.
// *
// *  NOTICE: You are permitted to use, modify, and distribute this file
// *  in accordance with the terms of the license agreement accompanying it.
// ******************************************************************************/

namespace elh.ui
{
	public abstract class URenderer : UView, IRenderer
	{
		protected bool dirty;

		public void SetDirty()
		{ //
			dirty = true;
		}

		/// <inheritdoc />
		public void Render()
		{
			dirty = false;
			OnWillRender();
			OnRender();
		}

		/// <inheritdoc />
		protected virtual void Start()
		{
			if (!dirty) return;
			dirty = false;
			OnRender();
		}

		/// <inheritdoc />
		protected virtual void OnEnable()
		{
			if (!dirty) return;
			dirty = false;
			OnRender();
		}

		/// <inheritdoc />
		protected virtual void Update()
		{
			if (!dirty) return;
			dirty = false;
			OnWillRender();
			OnRender();
		}

		/// <inheritdoc />
		protected virtual void LateUpdate()
		{
			if (!dirty) return;
			dirty = false;
			OnWillRender();
			OnRender();
		}

		/// <summary>
		/// Occurs immediately before rendering when new props or state are being received.
		/// Use this as an opportunity to perform preparation before an update occurs.
		/// This method is not called for the initial render.
		/// </summary>
		protected virtual void OnWillRender() { }

		protected abstract void OnRender();
	}
}
