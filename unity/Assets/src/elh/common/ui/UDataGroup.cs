﻿// *******************************************************************************
// *  Copyright (c) 2017 Max Stankevich. All Rights Reserved.
// *
// *  NOTICE: You are permitted to use, modify, and distribute this file
// *  in accordance with the terms of the license agreement accompanying it.
// ******************************************************************************/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace elh.ui
{
	public abstract class UDataGroup<T> : URenderer, IDataGroup<T>
	{
		protected readonly List<T> items;

		protected UDataGroup() { items = new List<T>(); }

		protected virtual bool DeactivateEmptyViews { get { return true; } }

		public IList<T> State { get { return items; } }

		/// <inheritdoc />
		public void SetState(IEnumerable<T> next)
		{
			if (!ReferenceEquals(next, items))
			{
				items.Clear();
				items.AddRange(next);
			}
			dirty = true;
			OnStateUpdate();
		}

		public void Attach(T item)
		{
			items.Add(item);
			dirty = true;
			OnStateUpdate();
		}

		public void Attach(ViewUpdater<T> factory)
		{
			var item = default(T);
			item = factory(item);
			items.Add(item);
			dirty = true;
			OnStateUpdate();
		}

		/// <inheritdoc />
		protected override void OnRender()
		{
			var index = 0;
			var views = GetViews();

			// activate existing views
			for (var imax = Mathf.Min(items.Count, views.Count); index < imax; index++)
			{
				var item = items[index];
				var view = (IView<T>)views[index];
				view.Active = true;
				SetViewState(view, item, index);
				view.Render();
			}

			// create views
			for (var imax = items.Count; index < imax; index++)
			{
				var item = items[index];
				var view = CreateView(item, index);
				InitializeView(view, item, index);
				SetViewState(view, item, index);
				views.Add(view);
				view.Render();
			}

			// deactivate other views
			var deactivateEmpty = DeactivateEmptyViews;
			for (var imax = views.Count; index < imax; index++)
			{
				var view = (IView<T>)views[index];
				SetViewState(view, default(T), index);
				if (deactivateEmpty)
					view.Active = false;
			}
		}

		public abstract IList GetViews();
		protected abstract IView<T> CreateView(T state, int index);

		/// <summary> Initialize view after creation. </summary>
		/// <param name="view"></param>
		/// <param name="state"></param>
		/// <param name="index"></param>
		protected virtual void InitializeView(IView<T> view, T state, int index)
		{
			view.Active = true;
		}

		/// <summary> Modify state of the view. </summary>
		/// <param name="view"></param>
		/// <param name="state"></param>
		/// <param name="index"></param>
		protected virtual void SetViewState(IView<T> view, T state, int index)
		{
			view.SetState(state);
		}

		/// <summary> Occurs when state has been updated. </summary>
		protected virtual void OnStateUpdate() { }
	}
}
