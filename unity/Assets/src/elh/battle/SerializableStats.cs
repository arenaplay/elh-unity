﻿// *******************************************************************************
// *  Copyright (c) 2017 Max Stankevich. All Rights Reserved.
// *
// *  NOTICE: You are permitted to use, modify, and distribute this file
// *  in accordance with the terms of the license agreement accompanying it.
// ******************************************************************************/

using System;
using System.Collections.Generic;
using UnityEngine;

namespace elh.battle
{
	[Serializable]
	public class SerializableStats : IStats
	{
		[SerializeField]
		protected List<Stat> actual = new List<Stat>();

		[SerializeField]
		protected List<Stat> totals = new List<Stat>();

		/// <inheritdoc />
		public double ActualOf(string key)
		{
			return ValueOf(actual, key);
		}

		/// <inheritdoc />
		public void SetActualOf(string key, double value)
		{
			SetValueOf(actual, key, value);
		}

		/// <inheritdoc />
		public void ModifyActualOf(string key, double delta)
		{
			Stat entry;
			var index = IndexOf(actual, key);
			if (index < 0)
			{
				entry.type = key;
				entry.value = (float)delta;
				actual.Add(entry);
			}
			else
			{
				entry = actual[index];
				entry.value += (float)delta;
				actual[index] = entry;
			}
		}

		/// <inheritdoc />
		public void SetTotalOf(string key, double value)
		{
			SetValueOf(totals, key, value);
		}

		/// <inheritdoc />
		public double TotalOf(string key)
		{
			return ValueOf(totals, key);
		}

		public void Clear()
		{
			actual.Clear();
			totals.Clear();
		}

		public void ToDefaults()
		{
			Clear();
			SetTotalOf(StatType.HEALTH, 100);
			SetActualOf(StatType.HEALTH, 100);
		}

		protected int IndexOf(IList<Stat> values, string type)
		{
			for (int i = 0, imax = values.Count; i < imax; i++)
				if (values[i].type == type)
					return i;

			return -1;
		}

		protected double ValueOf(IList<Stat> values, string type)
		{
			var index = IndexOf(values, type);
			if (index < 0) return 0;
			return values[index].value;
		}

		protected void SetValueOf(IList<Stat> values, string type, double value)
		{
			Stat entry;
			var index = IndexOf(values, type);
			if (index >= 0)
			{
				entry = values[index];
				entry.value = (float)value;
				values[index] = entry;
				return;
			}

			entry = new Stat();
			entry.type = type;
			entry.value = (float)value;
			values.Add(entry);
		}
	}
}
