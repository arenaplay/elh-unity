﻿// *******************************************************************************
// *  Copyright (c) 2017 Max Stankevich. All Rights Reserved.
// *
// *  NOTICE: You are permitted to use, modify, and distribute this file
// *  in accordance with the terms of the license agreement accompanying it.
// ******************************************************************************/

using elh.battle.api;

namespace elh.battle
{
	public class BattleUnit : IBattleUnit
	{
		protected IStats stats;

		/// <inheritdoc />
		public string Name { get; set; }

		/// <inheritdoc />
		public IStats Stats { get { return stats; } set { stats = value; } }

		/// <inheritdoc />
		public bool Dead { get; set; }
	}
}
