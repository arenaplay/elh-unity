﻿// *******************************************************************************
// *  Copyright (c) 2017 Max Stankevich. All Rights Reserved.
// *
// *  NOTICE: You are permitted to use, modify, and distribute this file
// *  in accordance with the terms of the license agreement accompanying it.
// ******************************************************************************/

using System.Collections.Generic;

namespace elh.battle.api
{
	public interface IAttack
	{
		void ResolveAffect(IBattleUnit attacker, IBattleUnit defender, ICollection<Stat> results);
	}
}
